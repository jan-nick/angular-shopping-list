import { AppConfig as devConfig } from "./environment.dev"

export const AppConfig = {
  production: false,
  environment: 'LOCAL',
  firebaseConfig: devConfig.firebaseConfig
};
