import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { fadeIn } from 'src/lib/animations/fade.animation';
import { AuthService } from '../../../services/auth.service';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss'],
  animations: [fadeIn],
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
  @Output()
  handleChangeAuthType: EventEmitter<'login'> = new EventEmitter<'login'>();

  form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
  });

  formChangesSubcription!: Subscription;

  authError!: string;

  emailInvalid: boolean = false;

  isLoading: boolean = false;
  hasSubmitted: boolean = false;

  private get email() {
    return this.form.get('email');
  }

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.formChangesSubcription?.unsubscribe();
  }

  initForm() {
    this.formChangesSubcription = this.form.valueChanges.subscribe((value) => {
      this.emailInvalid = !!this.email && this.email.dirty && !this.email.valid;
    });
  }

  async onSubmit() {
    if (this.form.invalid) return;

    this.isLoading = true;

    try {
      const email = this.email?.value;

      await this.authService.resetPassword(email);

      this.hasSubmitted = true;
    } catch (err) {
      this.authError = err && err.code;
    }

    this.isLoading = false;
  }
}
