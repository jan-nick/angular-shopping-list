import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { fadeIn } from 'src/lib/animations/fade.animation';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeIn],
})
export class LoginComponent implements OnInit {
  @Output() handleChangeAuthType: EventEmitter<'signup' | 'passwordReset'> =
    new EventEmitter<'signup' | 'passwordReset'>();

  form: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required]),
  });

  authError!: string;

  isUnknownError: boolean = false;
  emailInvalid: boolean = false;
  userNotFound: boolean = false;
  isWrongPassword: boolean = false;

  isLoading: boolean = false;

  private get email() {
    return this.form.get('email');
  }

  private get password() {
    return this.form.get('password');
  }

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form.valueChanges.subscribe((value) => {
      this.emailInvalid = !!this.email && this.email.dirty && !this.email.valid;
    });
  }

  async onSubmit() {
    if (this.form.invalid) return;

    this.isLoading = true;

    try {
      const email = this.email?.value;
      const password = this.password?.value;

      await this.authService.emailLogin(email, password);

      await this.router.navigate(['/home']);
    } catch (error) {
      this.setAuthErrors(error && error.code);
    }

    this.isLoading = false;
  }

  setAuthErrors(error: string) {
    this.userNotFound = error === 'auth/user-not-found';
    this.isWrongPassword = error === 'auth/wrong-password';
    this.isUnknownError = !this.userNotFound && !this.isWrongPassword;

    if (this.userNotFound) this.email?.setErrors({ incorrect: true });
    if (this.isWrongPassword) this.password?.setErrors({ incorrect: true });
  }
}
