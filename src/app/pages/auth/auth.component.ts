import { Component, OnInit } from '@angular/core';
import { fadeIn } from 'src/lib/animations/fade.animation';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  animations: [fadeIn],
})
export class AuthComponent implements OnInit {
  authError!: string;

  authType!: 'login' | 'signup' | 'passwordReset';

  isLogin: boolean = false;
  isSignup: boolean = false;
  isPasswordReset: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.changeAuthType('login');
  }

  changeAuthType(authType: 'login' | 'signup' | 'passwordReset') {
    this.authType = authType;

    this.isLogin = authType === 'login';
    this.isSignup = authType === 'signup';
    this.isPasswordReset = authType === 'passwordReset';
  }
}
