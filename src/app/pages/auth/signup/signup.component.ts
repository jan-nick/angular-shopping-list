import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import {
  FormControl,
  FormGroup,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { fadeIn } from 'src/lib/animations/fade.animation';
import { MIN_PASSWORD_LENGTH } from '../../../../lib/utilities/validation.util';
import { AuthService } from '../../../services/auth.service';

const passwordMatchValidator = (g: FormGroup) => {
  return g?.get('password')?.value === g?.get('passwordConfirm')?.value
    ? null
    : { mismatch: true };
};

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  animations: [fadeIn],
})
export class SignupComponent implements OnInit, OnDestroy {
  @Output()
  handleChangeAuthType: EventEmitter<'login'> = new EventEmitter<'login'>();

  form: FormGroup = new FormGroup(
    {
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.minLength(MIN_PASSWORD_LENGTH),
        Validators.required,
      ]),
      passwordConfirm: new FormControl('', []),
    },
    { validators: passwordMatchValidator as ValidatorFn }
  );

  formChangesSubcription!: Subscription;

  authError!: string;

  isUnknownError: boolean = false;
  emailInvalid: boolean = false;
  emailTaken: boolean = false;
  passwordInvalid: boolean = false;
  passwordDoesMatch: boolean = false;

  isLoading: boolean = false;

  readonly MIN_PASSWORD_LENGTH = MIN_PASSWORD_LENGTH;

  private get email() {
    return this.form.get('email');
  }

  private get password() {
    return this.form.get('password');
  }

  private get passwordConfirm() {
    return this.form.get('passwordConfirm');
  }

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.initForm();
  }

  ngOnDestroy() {
    this.formChangesSubcription?.unsubscribe();
  }

  initForm() {
    this.formChangesSubcription = this.form.valueChanges.subscribe((value) => {
      this.emailInvalid = !!this.email && this.email.dirty && !this.email.valid;
      this.passwordInvalid =
        !!this.password && this.password.dirty && !this.password.valid;
      this.passwordDoesMatch =
        this.password?.value === this.passwordConfirm?.value;
    });
  }

  async onSubmit() {
    if (this.form.invalid) return;

    this.isLoading = true;

    try {
      const email = this.email?.value;
      const password = this.password?.value;

      if (!this.passwordDoesMatch) throw Error('password-not-matching');

      const userData = await this.authService.emailSignUp(email, password);
      await this.authService.setInitialUserData({ ...userData });

      await this.router.navigate(['/home']);
    } catch (err) {
      this.setAuthErrors(err && err.code);
    }

    this.isLoading = false;
  }

  setAuthErrors(error: string) {
    this.emailTaken = error === 'auth/email-already-in-use';
    this.isUnknownError = !this.emailTaken;

    if (this.emailTaken) this.email?.setErrors({ incorrect: true });
  }
}
