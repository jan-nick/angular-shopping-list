import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ShoppingListService } from 'src/app/services/shopping-list.service';
import { UserService } from 'src/app/services/user.service';
import { IshoppingList } from 'src/lib/models/interfaces/IshoppingList.type';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.scss'],
})
export class AddListComponent implements OnInit {
  shoppingListName: string = '';

  isLoading: boolean = false;

  constructor(
    private matDialogRef: MatDialogRef<AddListComponent>,
    private router: Router,
    private shoppingListService: ShoppingListService,
    private userService: UserService
  ) {}

  ngOnInit(): void {}

  async onPressSave() {
    this.isLoading = true;

    const { id } = await this.createShoppingList();

    this.matDialogRef.close();
    
    await this.router.navigate(['/shopping-list'], {
      queryParams: { shoppingListId: id },
    });

    this.isLoading = false;
  }

  async createShoppingList() {
    return await this.shoppingListService.saveShoppingList({
      name: this.shoppingListName,
      userId: this.userService.currentUser?.uid,
    } as IshoppingList);
  }
}
