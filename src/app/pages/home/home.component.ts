import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { ShoppingListService } from 'src/app/services/shopping-list.service';
import { UserService } from 'src/app/services/user.service';
import { fadeIn } from 'src/lib/animations/fade.animation';
import { IshoppingList } from 'src/lib/models/interfaces/IshoppingList.type';
import { AddListComponent } from './add-list/add-list.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [fadeIn]
})
export class HomeComponent implements OnInit {
  shoppingLists$!: Observable<IshoppingList[]>;

  constructor(
    private matDialog: MatDialog,
    private shoppingListService: ShoppingListService,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userService.user$.pipe(take(1)).subscribe((user) => {
      this.shoppingLists$ = this.shoppingListService.getUserShoppingLists(
        <string>user?.uid
      );
    });
  }

  presentAddListDialog() {
    const dialog = this.matDialog.open(AddListComponent);
  }
}
