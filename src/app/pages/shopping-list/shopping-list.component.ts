import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { ShoppingListService } from 'src/app/services/shopping-list.service';
import { UserService } from 'src/app/services/user.service';
import { fadeIn } from 'src/lib/animations/fade.animation';
import {
  IshoppingList,
  IshoppingListItem,
} from 'src/lib/models/interfaces/IshoppingList.type';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
  animations: [fadeIn],
})
export class ShoppingListComponent implements OnInit, OnDestroy {
  shoppingList!: IshoppingList;
  shoppingListSubscription!: Subscription;

  isLoading: boolean = false;
  isAddingNewItem: boolean = false;

  constructor(
    private shoppingListService: ShoppingListService,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getShoppingList();
  }

  ngOnDestroy() {
    this.shoppingListSubscription?.unsubscribe();
  }

  getShoppingList() {
    this.isLoading = true;

    this.route.params.pipe(take(1)).subscribe((params) => {
      this.userService.user$.pipe(take(1)).subscribe((user) => {
        this.shoppingListSubscription = this.shoppingListService
          .getShoppingListById(params.shoppingListId)
          .subscribe((shoppingList) => {
            this.shoppingList = shoppingList as IshoppingList;
            this.isLoading = false;
          });
      });
    });
  }

  async onItemDelete(shoppingListItem: IshoppingListItem) {
    this.shoppingList.items = this.shoppingList.items?.filter(
      (item) => item.id !== shoppingListItem.id
    );
    await this.shoppingListService.updateShoppingList(this.shoppingList);
  }

  async onItemUpdate(shoppingListItem: IshoppingListItem) {
    for (let item of <IshoppingListItem[]>this.shoppingList.items) {
      if (item.id !== shoppingListItem.id) continue;
      item = shoppingListItem;
      break;
    }
    await this.shoppingListService.updateShoppingList(this.shoppingList);
  }

  addShoppingListItem() {
    this.isAddingNewItem = true;
  }

  onCancelAddNewItem() {
    this.isAddingNewItem = false;
  }

  async onNewItemSave(shoppingListItem: IshoppingListItem) {
    this.isAddingNewItem = false;

    if (!this.shoppingList.items) this.shoppingList.items = [];
    this.shoppingList.items?.push({
      ...shoppingListItem,
      isChecked: false,
      id: this.shoppingListService.createId(),
    });
    await this.shoppingListService.updateShoppingList(this.shoppingList);
  }
}
