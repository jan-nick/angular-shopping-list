import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { fadeIn } from 'src/lib/animations/fade.animation';
import { IshoppingListItem } from 'src/lib/models/interfaces/IshoppingList.type';

@Component({
  selector: 'app-shopping-list-item',
  templateUrl: './shopping-list-item.component.html',
  styleUrls: ['./shopping-list-item.component.scss'],
  animations: [fadeIn],
})
export class ShoppingListItemComponent implements OnInit {
  @Input() shoppingListItem: IshoppingListItem = {} as IshoppingListItem;
  @Input() isNewItem: boolean = false;

  @Output() updateItemChange: EventEmitter<IshoppingListItem> =
    new EventEmitter<IshoppingListItem>();
  @Output() deleteItemChange: EventEmitter<IshoppingListItem> =
    new EventEmitter<IshoppingListItem>();

  @ViewChild('itemNameInput')
  private itemNameInput!: ElementRef<HTMLInputElement>;

  isEditing: boolean = false;

  constructor() {}

  ngOnInit(): void {
    this.isEditing = this.isNewItem;
    if (this.isEditing) {
      this.focusItemNameInput();
    }
  }

  onChangeCheck($event: MatCheckboxChange) {
    this.shoppingListItem.isChecked = $event.checked;
    this.updateItemChange.emit(this.shoppingListItem);
  }

  onClickItemName() {
    this.isEditing = true;
    this.focusItemNameInput();
  }

  onClickEditNameDone() {
    this.isEditing = false;
    this.updateItemChange.emit(this.shoppingListItem);
  }

  focusItemNameInput() {
    setTimeout(() => {
      this.itemNameInput.nativeElement.focus();
    }, 0);
  }
}
