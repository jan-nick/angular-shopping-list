import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShoppingListComponent } from './shopping-list.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ShoppingListRoutingModule } from './home-routing.module';
import { ShoppingListItemComponent } from './shopping-list-item/shopping-list-item.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [ShoppingListComponent, ShoppingListItemComponent],
  imports: [
    CommonModule,
    ShoppingListRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class ShoppingListModule { }
