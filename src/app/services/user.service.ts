import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Iuser } from '../../lib/models/interfaces/Iuser.type';
import { Observable, of } from 'rxjs';
import { switchMap, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user$: Observable<Iuser | null | undefined>;
  currentUser!: Iuser | null | undefined;

  constructor(private db: AngularFirestore, private afAuth: AngularFireAuth) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap((user) => {
        if (user) {
          return this.db.doc<Iuser>(`users/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
    this.user$.subscribe((user) => {
      this.currentUser = user;
    });
  }

  getUser(userId: string) {
    return this.db
      .collection('users')
      .doc<Iuser>(userId)
      .valueChanges()
      .pipe(first());
  }

  async updateUser(user: Iuser) {
    return await this.db.collection('users').doc<Iuser>(user.uid).update(user);
  }

  async setUserData(user: Iuser) {
    const userRef: AngularFirestoreDocument<Iuser> = this.db.doc(
      `users/${user.uid}`
    );

    const data: Iuser = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName || '',
    };

    console.log(user);

    return userRef.set(data, { merge: true }).then((credential) => {
      return this.db.doc<Iuser>(`users/${user.uid}`).valueChanges();
    });
  }
}
