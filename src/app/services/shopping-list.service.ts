import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import {
  IshoppingList,
  IshoppingListItem,
} from 'src/lib/models/interfaces/IshoppingList.type';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root',
})
export class ShoppingListService {
  constructor(private db: AngularFirestore, private userService: UserService) {}

  async saveShoppingList(shoppingList: IshoppingList) {
    const id = this.db.createId();
    const data = { ...shoppingList, id };

    await this.db.collection<IshoppingList>('shopping-lists').doc(id).set(data);

    return data;
  }

  getUserShoppingLists(userId: string) {
    return this.db
      .collection<IshoppingList>('shopping-lists', (ref) =>
        ref.where('userId', '==', userId)
      )
      .valueChanges();
  }

  getShoppingListById(shoppingListId: string) {
    return this.db
      .collection<IshoppingList>('shopping-lists')
      .doc(shoppingListId)
      .valueChanges();
  }

  updateShoppingList(shoppingList: IshoppingList) {
    return this.db
      .collection<IshoppingList>('shopping-lists')
      .doc(shoppingList.id)
      .update({ ...shoppingList });
  }

  createId() {
    return this.db.createId();
  }
}
