import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';
import { UserService } from './user.service';
import { Iuser } from '../../lib/models/interfaces/Iuser.type';
import firebase from 'firebase';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isAuthenticated$: Observable<boolean>;
  signIn$: Observable<firebase.User | null>;

  constructor(
    private afAuth: AngularFireAuth,
    private db: AngularFirestore,
    private userService: UserService
  ) {
    this.isAuthenticated$ = this.afAuth.authState.pipe(
      switchMap((user) => {
        return of(!!user);
      })
    );
    this.signIn$ = this.afAuth.authState.pipe(
      filter((user) => {
        return !!user;
      })
    );
  }

  async emailLogin(email: string, password: string) {
    const { user, credential } = await this.afAuth.signInWithEmailAndPassword(
      email,
      password
    );

    return { credential, user };
  }

  async emailSignUp(email: string, password: string) {
    const credential = await this.getUserCredential(email, password);
    const { user } = await this.afAuth.createUserWithEmailAndPassword(
      email,
      password
    );
    console.log(credential);
    return { credential, user };
  }

  async resetPassword(email: string) {
    // todo
  }

  async signOut() {
    return await this.afAuth.signOut();
  }

  async setInitialUserData(userData: {
    credential: firebase.auth.AuthCredential;
    user: firebase.User | null;
  }) {
    if (!userData.user?.email) throw Error('Email cannot be empty');
    const user: Iuser = {
      uid: userData.user.uid,
      email: userData.user.email,
    };
    console.log(userData);
    return await this.userService.setUserData(user);
  }

  async getUserCredential(email?: string, password?: string) {
    let credential: firebase.auth.AuthCredential;
    if (email && password) {
      credential = firebase.auth.EmailAuthProvider.credential(email, password);
    } else {
      credential = (await this.afAuth.getRedirectResult()).credential!;
    }
    return credential;
  }
}
