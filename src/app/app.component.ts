import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  isLoadingSignOut: boolean = false;
  constructor(
    public authService: AuthService,
    private router: Router,
    private translate: TranslateService
  ) {
    this.translate.setDefaultLang('en');
    this.translate.use('en');
  }

  async signOut() {
    this.isLoadingSignOut = true;
    await this.authService.signOut();
    await this.router.navigate(['/auth']);
    this.isLoadingSignOut = false;
  }
}
