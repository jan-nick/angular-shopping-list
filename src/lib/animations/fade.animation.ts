import {
  trigger,
  state,
  transition,
  animate,
  style,
} from '@angular/animations';

export const fadeIn = trigger('fadeIn', [
  state('in', style({ opacity: 1 })),

  transition(':enter', [style({ opacity: 0 }), animate('240ms ease')]),
]);

export const fadeInSlow = trigger('fadeInSlow', [
  state('in', style({ opacity: 1 })),

  transition(':enter', [style({ opacity: 0 }), animate('480ms ease')]),
]);
