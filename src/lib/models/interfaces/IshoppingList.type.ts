export interface IshoppingList {
  userId: string;
  id: string;
  name: string;
  items?: IshoppingListItem[];
}

export interface IshoppingListItem {
  id: string;
  name: string;
  quantity: number;
  isChecked: boolean;
}
