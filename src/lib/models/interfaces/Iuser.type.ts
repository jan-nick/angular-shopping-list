export interface Iuser {
  uid: string;

  email: string;
  displayName?: string;
}