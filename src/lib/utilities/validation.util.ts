import {
  FormControl,
  FormGroup,
  FormGroupDirective,
  NgForm,
} from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

export const MIN_PASSWORD_LENGTH = 8;

export class SubmitErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(
    control: FormControl | null,
    formGroup: FormGroupDirective | NgForm | null
  ): boolean {
    if (!control || !formGroup) return false;
    return control?.invalid && formGroup?.submitted;
  }
}
